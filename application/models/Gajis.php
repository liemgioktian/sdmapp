<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gajis extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'gaji';
    $this->thead = array(
      (object) array('mData' => 'orders', 'sTitle' => 'No', 'visible' => false),
      (object) array('mData' => 'nama', 'sTitle' => 'Karyawan'),
      (object) array('mData' => 'gapok', 'sTitle' => 'Gaji Pokok')
    );
    $this->form = array (
        array (
		      'name' => 'karyawan',
		      'label'=> 'Nama',
		      'options' => array(),
		      'width' => 2,
		      'attributes' => array(
		        array('data-autocomplete' => 'true'),
		        array('data-model' => 'Karyawans'),
		        array('data-field' => 'nama'),
		        array('disabled' => true)
			    )),
        array (
		      'name' => 'gapok',
		      'label'=> 'Gaji Pokok',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'hari_kerja_per_bulan',
		      'label'=> 'Hari Kerja',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'uang_makan',
		      'label'=> 'Uang Makan',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'kompensasi',
		      'label'=> 'Kompensasi',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'tunjangan_lokasi',
		      'label'=> 'Tunjangan Lokasi',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'tunjangan_jabatan',
		      'label'=> 'Tunjangan Jabatan',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'lembur',
		      'label'=> 'Lembur',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'bpjs_tk',
		      'label'=> 'BPJS Tenaga Kerja',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'simpan_pinjam',
		      'label'=> 'Simpan Pinjam',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'konsinyasi',
		      'label'=> 'Konsinyasi',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'pot_toko',
		      'label'=> 'POT Toko',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'palugada',
		      'label'=> 'Palugada',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'emergency',
		      'label'=> 'Emergency',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
    );
    $this->childs = array (
    );
  }

  function dt () {
    $this->datatables
      ->select("{$this->table}.uuid")
      ->select("{$this->table}.orders")
      ->select('karyawan.nama')
      ->select("CONCAT('Rp. ', FORMAT(gapok, 0)) gapok")
      ->join('karyawan', 'karyawan.uuid = gaji.karyawan');
    return parent::dt();
  }

}