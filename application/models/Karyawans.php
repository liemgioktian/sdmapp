<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawans extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'karyawan';
    $this->thead = array(
      (object) array('mData' => 'orders', 'sTitle' => 'No', 'visible' => false),
      (object) array('mData' => 'nik', 'sTitle' => 'NIK'),
      (object) array('mData' => 'no_ktp', 'sTitle' => 'No. KTP'),
      (object) array('mData' => 'nama', 'sTitle' => 'Nama'),
      (object) array('mData' => 'nama_bagian', 'sTitle' => 'Bagian', 'searchable' => false)
    );
    $this->form = array (
        array (
				      'name' => 'nik',
				      'width' => 2,
		      		'label'=> 'NIK',
					  ),
        array (
				      'name' => 'kontrak_kerja',
				      'label'=> 'Kontrak Kerja',
				      'width' => 2,
		      		'options' => array(
                array('text' => 'KOPERSEMAR', 'value' => 'KOPERSEMAR'),
                array('text' => 'PT SEM', 'value' => 'PT SEM'),
				      )
					  ),
        array (
				      'name' => 'nama',
				      'width' => 2,
		      		'label'=> 'Nama',
					  ),
        array (
				      'name' => 'no_kk',
				      'width' => 2,
		      		'label'=> 'No. KK',
					  ),
        array (
				      'name' => 'status_perkawinan',
				      'label'=> 'Status Perkawinan',
				      'width' => 2,
		      		'options' => array(
                array('text' => 'TIDAK KAWIN', 'value' => 'TIDAK KAWIN'),
                array('text' => 'KAWIN', 'value' => 'KAWIN'),
				      )
					  ),
        array (
		      'name' => 'jumlah_anak',
		      'label'=> 'Jumlah Anak',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
				      'name' => 'no_ktp',
				      'width' => 2,
		      		'label'=> 'No. KTP',
					  ),
        array (
				      'name' => 'alamat',
				      'width' => 2,
		      		'label'=> 'Alamat',
					  ),
        array (
				      'name' => 'tempat_lahir',
				      'width' => 2,
		      		'label'=> 'Tempat Lahir',
					  ),
        array (
		      'name' => 'tanggal_lahir',
		      'label'=> 'Tanggal Lahir',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
		      'name' => 'tanggal_masuk',
		      'label'=> 'Tanggal Masuk',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
		      'name' => 'bagian',
		      'label'=> 'Bagian',
		      'options' => array(),
		      'width' => 2,
		      'attributes' => array(
		        array('data-autocomplete' => 'true'),
		        array('data-model' => 'Bagians'),
		        array('data-field' => 'nama')
			    )),
    );
    $this->childs = array (
    );
  }

  function dt () {
    $this->datatables
      ->select("{$this->table}.uuid")
      ->select("{$this->table}.orders")
      ->select('karyawan.nama')
      ->select('nik')
      ->select('no_ktp')
      ->select('bagian.nama nama_bagian', false)
      ->join('bagian', 'karyawan.bagian = bagian.uuid', 'left');
    return parent::dt();
  }

  function create ($record) {
  	$created = $this->findOne(parent::create($record));
  	$this->load->model('Users');
  	$created['user'] = $this->Users->afterKaryawanCreated($created);
  	$this->update($created);
  	return $created['uuid'];
  }

  function karyawanRedirectDataDiri()
  {
  	$this->load->model('Roles');
  	$role = $this->Roles->findOne(array('name' => 'karyawan'));
  	if ($role['uuid'] === $this->session->userdata('role'))
  	{
  		$karyawan = $this->findOne(array('user' => $this->session->userdata('uuid')));
  		return $karyawan['uuid'];
  	}
  	else return false;
  }

  function karyawanRedirectDataGaji ()
  {
  	$this->load->model('Roles');
  	$role = $this->Roles->findOne(array('name' => 'karyawan'));
  	if ($role['uuid'] === $this->session->userdata('role'))
  	{
  		$karyawan = $this->findOne(array('user' => $this->session->userdata('uuid')));
  		$this->load->model('Gajis');
  		$gaji = $this->Gajis->findOne(array('karyawan' => $karyawan['uuid']));
  		return $gaji['uuid'];
  	}
  	else return false;
  }

  function update ($record) {
    $record = $this->savechild($record);
    $record['updatedAt'] = date('Y-m-d H:i:s');
    $this->db->where('uuid', $record['uuid'])->update($this->table, $record);
    return $record['uuid'];
  }

}