<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends MY_Controller {

	function __construct ()
	{
		$this->model = 'Karyawans';
		parent::__construct();
	}

  public function index () {
    $model = $this->model;
    if ($post = $this->$model->lastSubmit($this->input->post())) {
      if (isset ($post['delete'])) $this->$model->delete($post['delete']);
      else {
          $db_debug = $this->db->db_debug;
          $this->db->db_debug = FALSE;

          $result = $this->$model->save($post);

          $error = $this->db->error();
          $this->db->db_debug = $db_debug;
          if (isset ($result['error'])) $error = $result['error'];
          if(count($error)){
              $this->session->set_flashdata('model_error', $error['message']);
              redirect($this->controller);
          }
      }
    }

  	$karyawanUuid = $this->$model->karyawanRedirectDataDiri();
  	if (false === $karyawanUuid)
    {
      $vars = array();
      $vars['page_name'] = 'table';
      $vars['js'] = array(
        'jquery.dataTables.min.js',
        'dataTables.bootstrap4.js',
        'table.js'
      );
      $vars['thead'] = $this->$model->thead;
      $this->loadview('index', $vars);
    }
  	else redirect(site_url("Karyawan/read/{$karyawanUuid}"));
  }

}