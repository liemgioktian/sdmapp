<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gaji extends MY_Controller {

	function __construct ()
	{
		$this->model = 'Gajis';
		parent::__construct();
	}

  public function index () {
  	$this->load->model('Karyawans');
  	$gajiUuid = $this->Karyawans->karyawanRedirectDataGaji();
  	if (false === $gajiUuid) parent::index();
  	else redirect(site_url("Gaji/read/{$gajiUuid}"));
  }

}