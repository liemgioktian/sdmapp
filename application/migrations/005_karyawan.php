<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_karyawan extends CI_Migration {

  function up () {

    $this->db->query("
      CREATE TABLE `karyawan` (
        `uuid` varchar(255) NOT NULL,
        `orders` INT(11) UNIQUE NOT NULL AUTO_INCREMENT,
        `createdAt` datetime DEFAULT NULL,
        `updatedAt` datetime DEFAULT NULL,
        `user` varchar(255) NOT NULL,
        `nik` varchar(255) NOT NULL,
        `kontrak_kerja` varchar(255) NOT NULL,
        `nama` varchar(255) NOT NULL,
        `no_kk` varchar(255) NOT NULL,
        `status_perkawinan` varchar(255) NOT NULL,
        `jumlah_anak` INT(11) NOT NULL,
        `no_ktp` varchar(255) NOT NULL,
        `alamat` varchar(255) NOT NULL,
        `tempat_lahir` varchar(255) NOT NULL,
        `tanggal_lahir` DATE NOT NULL,
        `tanggal_masuk` DATE NOT NULL,
        `bagian` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`),
        KEY `bagian` (`bagian`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

  }

  function down () {
    $this->db->query("DROP TABLE IF EXISTS `karyawan`");
  }

}