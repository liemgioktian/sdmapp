<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_gaji extends CI_Migration {

  function up () {

    $this->db->query("
      CREATE TABLE `gaji` (
        `uuid` varchar(255) NOT NULL,
        `orders` INT(11) UNIQUE NOT NULL AUTO_INCREMENT,
        `createdAt` datetime DEFAULT NULL,
        `updatedAt` datetime DEFAULT NULL,
        `karyawan` varchar(255) NOT NULL,
        `gapok` INT(11) NOT NULL,
        `hari_kerja_per_bulan` INT(11) NOT NULL,
        `uang_makan` INT(11) NOT NULL,
        `kompensasi` INT(11) NOT NULL,
        `tunjangan_lokasi` INT(11) NOT NULL,
        `tunjangan_jabatan` INT(11) NOT NULL,
        `lembur` INT(11) NOT NULL,
        `bpjs_tk` INT(11) NOT NULL,
        `simpan_pinjam` INT(11) NOT NULL,
        `konsinyasi` INT(11) NOT NULL,
        `pot_toko` INT(11) NOT NULL,
        `palugada` INT(11) NOT NULL,
        `emergency` INT(11) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

  }

  function down () {
    $this->db->query("DROP TABLE IF EXISTS `gaji`");
  }

}